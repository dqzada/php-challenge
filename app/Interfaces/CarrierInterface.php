<?php

namespace App\Interfaces;

use App\Contact,
	App\Call;


interface CarrierInterface
{
	public function dialContact(Contact $contact);

	public function makeCall(): Call;
}