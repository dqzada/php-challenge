<?php

namespace Tests;

use Mockery as m;
use PHPUnit\Framework\TestCase,
	App\Mobile,
	App\Provider;

class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$provider = new Provider;

		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName(''));
	}

}
