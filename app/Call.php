<?php

namespace App;


class Call
{
	
	function __construct(String $msisdn)
	{
		echo "Calling {$msisdn}...";
		return $this;
	}
}