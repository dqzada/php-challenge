<?php

namespace App;

use App\Contact,
    App\Call,
    App\Interfaces\CarrierInterface;

class Provider implements CarrierInterface {

    private $msisdn;
    public function dialContact(Contact $contact) {
        $this->msisdn = $contact->msisdn;
    }

    public function makeCall(): Call {
        return new Call($this->msisdn);
    }
}